//
//  GameScene.swift
//  Pachinko
//
//  Created by Hariharan S on 26/05/24.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    // MARK: - Properties

    var scoreLabel: SKLabelNode!
    var editLabel: SKLabelNode!
    var score = 0 {
        didSet {
            scoreLabel.text = "Score: \(score)"
        }
    }
    var editingMode: Bool = false {
        didSet {
            if self.editingMode {
                self.editLabel.text = "Done"
            } else {
                self.editLabel.text = "Edit"
            }
        }
    }
    let balls: [String] = [
        "ballBlue",
        "ballCyan",
        "ballGreen",
        "ballGrey",
        "ballPurple",
        "ballRed",
        "ballYellow"
    ]
    
    override func didMove(to view: SKView) {
        let background = SKSpriteNode(imageNamed: "background.jpg")
        background.position = CGPoint(x: 512, y: 384)
        background.blendMode = .replace
        background.zPosition = -1
        self.addChild(background)
        self.physicsWorld.contactDelegate = self
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        
        self.makeSlot(at: CGPoint(x: 128, y: 0), isGood: true)
        self.makeSlot(at: CGPoint(x: 384, y: 0), isGood: false)
        self.makeSlot(at: CGPoint(x: 640, y: 0), isGood: true)
        self.makeSlot(at: CGPoint(x: 896, y: 0), isGood: false)
        
        self.makeBouncer(at: CGPoint(x: 0, y: 0))
        self.makeBouncer(at: CGPoint(x: 256, y: 0))
        self.makeBouncer(at: CGPoint(x: 512, y: 0))
        self.makeBouncer(at: CGPoint(x: 768, y: 0))
        self.makeBouncer(at: CGPoint(x: 1024, y: 0))
        
        self.scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        self.scoreLabel.text = "Score: 0"
        self.scoreLabel.horizontalAlignmentMode = .right
        self.scoreLabel.position = CGPoint(x: 980, y: 700)
        self.addChild(scoreLabel)
        
        self.editLabel = SKLabelNode(fontNamed: "Chalkduster")
        self.editLabel.text = "Edit"
        self.editLabel.position = CGPoint(x: 80, y: 700)
        self.addChild(editLabel)
    }
    
    override func touchesBegan(
        _ touches: Set<UITouch>,
        with event: UIEvent?
    ) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            
            let objects = nodes(at: location)
            if objects.contains(self.editLabel) {
                self.editingMode.toggle()
            } else {
                if self.editingMode {
                    let size = CGSize(
                        width: Int.random(in: 16...128),
                        height: 16
                    )
                    let box = SKSpriteNode(
                        color: UIColor(
                            red: CGFloat.random(in: 0...1),
                            green: CGFloat.random(in: 0...1),
                            blue: CGFloat.random(in: 0...1),
                            alpha: 1
                        ),
                        size: size
                    )
                    box.zRotation = CGFloat.random(in: 0...3)
                    box.position = location
                    box.physicsBody = SKPhysicsBody(rectangleOf: box.size)
                    box.physicsBody?.isDynamic = false
                    self.addChild(box)
                } else if location.y > 500 {
                    let ball = SKSpriteNode(imageNamed: self.balls.randomElement() ?? "ballRed")
                    ball.name = "ball"
                    ball.physicsBody = SKPhysicsBody(
                        circleOfRadius: ball.size.width / 2.0
                    )
                    ball.physicsBody!.contactTestBitMask = ball.physicsBody!.collisionBitMask
                    ball.physicsBody?.restitution = 0.8
                    ball.position = location
                    self.addChild(ball)
                }
            }
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        guard let nodeA = contact.bodyA.node,
              let nodeB = contact.bodyB.node
        else {
            return
        }
        
        if nodeA.name == "ball" {
            collisionBetween(ball: nodeA, object: nodeB)
        } else if nodeB.name == "ball" {
            collisionBetween(ball: nodeB, object: nodeA)
        }
    }
}


// MARK: - Private Methods

private extension GameScene {
    func makeBouncer(at position: CGPoint) {
        let bouncer = SKSpriteNode(imageNamed: "bouncer")
        bouncer.position = position
        bouncer.physicsBody = SKPhysicsBody(
            circleOfRadius: bouncer.size.width / 2.0
        )
        bouncer.physicsBody?.isDynamic = false
        self.addChild(bouncer)
    }
    
    func makeSlot(
        at position: CGPoint,
        isGood: Bool
    ) {
        var slotBase: SKSpriteNode
        var slotGlow: SKSpriteNode
        
        if isGood {
            slotBase = SKSpriteNode(imageNamed: "slotBaseGood")
            slotGlow = SKSpriteNode(imageNamed: "slotGlowGood")
            slotBase.name = "good"
        } else {
            slotBase = SKSpriteNode(imageNamed: "slotBaseBad")
            slotGlow = SKSpriteNode(imageNamed: "slotGlowBad")
            slotBase.name = "bad"
        }
        
        slotBase.position = position
        slotGlow.position = position
        
        slotBase.physicsBody = SKPhysicsBody(rectangleOf: slotBase.size)
        slotBase.physicsBody?.isDynamic = false
        
        self.addChild(slotBase)
        self.addChild(slotGlow)
        
        let spin = SKAction.rotate(byAngle: .pi, duration: 10)
        let spinForever = SKAction.repeatForever(spin)
        slotGlow.run(spinForever)
    }
    
    func collisionBetween(ball: SKNode, object: SKNode) {
        if object.name == "good" {
            self.destroy(ball: ball)
            self.score += 1
            } else if object.name == "bad" {
                self.destroy(ball: ball)
                self.score -= 1
            }
    }
    
    func destroy(ball: SKNode) {
        if let fireParticles = SKEmitterNode(fileNamed: "FireParticles") {
            fireParticles.position = ball.position
            self.addChild(fireParticles)
        }
        ball.removeFromParent()
    }
}
